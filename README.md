## Introduction

13 resembles Android T. This repository is source for the NikGapps builds. Following are the source of these apks
- Playstore
- Android dump

## Directory Structure

NikGapps source follows below hierarchy  

    - AppSet1  
        --> Package1  
            --> ___Folder-Path-To-File___SubFolder-Path-To-File (___ 3 underscores represents directory separator)  
                --> File1.extn  
                --> File2.extn  
        --> Package2  
            --> ___Folder-Path-To-File___SubFolder-Path-To-File  
                --> File1.extn  
                --> File2.extn 
                 
    - AppSet2  
        --> Package1  
            --> ___Folder-Path-To-File___SubFolder-Path-To-File  
                --> File1.extn  
                --> File2.extn  
                --> File2.extn 
                
    - SetupWizard
        --> SetupWizard
            --> ___priv-app___SetupWizard (resembles as /priv-app/SetupWizard)
                --> SetupWizardPrebuilt.apk
            --> ___etc___permissions (resembles as /etc/permissions)
                --> com.google.android.setupwizard.xml
            --> ___overlay
                --> GmsConfigOverlayComms.apk
                --> GmsConfigOverlayGSA.apk
                --> GmsContactsProviderOverlay.apk
        --> GoogleOneTimeInitializer
            --> ___priv-app___GoogleOneTimeInitializer
                --> GoogleOneTimeInitializer.apk
            --> ___etc___permissions
                --> com.google.android.onetimeinitializer.xml
        --> GoogleRestore
            --> ___priv-app___GoogleRestore
                --> GoogleRestore.apk
            --> ___etc___permissions
                --> com.google.android.apps.restore.xml
